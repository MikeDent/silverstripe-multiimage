<?php
// Data Exetension allowing an image field to hold many images - for instance for a scrolling banner.
class MultiImage extends DataExtension
{
    private static $belongs_many = array("Page"=>"Page");
}
