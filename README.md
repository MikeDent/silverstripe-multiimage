# Silverstripe/MultiImage

Silverstripe module to allow an image field to hold multiple images, perhaps for an image gallery or animated banner.
